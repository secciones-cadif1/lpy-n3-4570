
acum_sueldo = 0 # se inicializa antes del ciclo
for i in range(1,6):
    print("Introduzca el sueldo del trabajador ",i,":")
    sueldo = float(input())

    acum_sueldo = acum_sueldo + sueldo

# fuera del ciclo
promedio_sueldos = acum_sueldo / 5 # la cantidad de trabajadores
print("El total de sueldos es:",acum_sueldo)
print("El promedio de los sueldos es ",promedio_sueldos)