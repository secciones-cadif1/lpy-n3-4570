# se inicializa antes del ciclo
cont = 0
cont_may_1000 = 0
acum_sueldo = 0 
acum_años = 0
acum_sueldo_mas_10 = 0
cont_mas_10_años = 0
while True:
    cont = cont + 1 # contador general

    print("Introduzca el sueldo del trabajador ",cont,":")
    sueldo = float(input())
    años = int(input("Introduzca la cantidad de años de servicio:"))

    if sueldo > 1000:
        cont_may_1000 = cont_may_1000 + 1

    if años > 10:
        acum_sueldo_mas_10 = acum_sueldo_mas_10 + sueldo
        cont_mas_10_años=cont_mas_10_años+1

    acum_años = acum_años + años
    acum_sueldo = acum_sueldo + sueldo

    resp = input("Desea continuar?")
    if resp.upper()=="N":
        break

# fuera del ciclo
if cont_mas_10_años == 0:
    prom_sueldo_mas_10_años = 0
else:
    prom_sueldo_mas_10_años = acum_sueldo_mas_10 / cont_mas_10_años
    
prom_años_servicio = acum_años / cont
porc_mas_10_años = cont_mas_10_años / cont *100
porce_may_1000 = cont_may_1000 / cont * 100
promedio_sueldos = acum_sueldo / cont # la cantidad de trabajadores

print("El total de sueldos es:",acum_sueldo)
print("El promedio de los sueldos es ",promedio_sueldos)
print("El promedio de años de servicios es ",prom_años_servicio," años ")
print("El porcentaje de trabajadores con mas de 10 años es ",porc_mas_10_años,"%")
print("El porcentaje de trabajadores que ganan mas de 1000 es ",porce_may_1000,"%")