# se inicializa antes del ciclo
cont = 0
acum_sueldo = 0 
while True:
    cont = cont + 1

    print("Introduzca el sueldo del trabajador ",cont,":")
    sueldo = float(input())

    acum_sueldo = acum_sueldo + sueldo

    resp = input("Desea continuar?")
    if resp.upper()=="N":
        break

# fuera del ciclo
promedio_sueldos = acum_sueldo / cont # la cantidad de trabajadores
print("El total de sueldos es:",acum_sueldo)
print("El promedio de los sueldos es ",promedio_sueldos)