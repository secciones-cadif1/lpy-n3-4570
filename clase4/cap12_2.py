n = int(input("Introduzca el numero de trabajadores:"))
if n<=0:
    print("debe ser un valor mayor a 0")
else:
    acum_sueldo = 0 # se inicializa antes del ciclo

    for i in range(1,n+1):
        print("Introduzca el sueldo del trabajador ",i,":")
        sueldo = float(input())

        acum_sueldo = acum_sueldo + sueldo

    # fuera del ciclo
    promedio_sueldos = acum_sueldo / n # la cantidad de trabajadores
    print("El total de sueldos es:",acum_sueldo)
    print("El promedio de los sueldos es ",promedio_sueldos)