import random

# los parametros actuales pueden ser valores
numero = random.randint(1,5) 

# tambien pueden ser variables
minimo = 1
maximo = 5
numero = random.randint(minimo,maximo) 

# tambien pueden ser variables
x = 1
y = 5
numero = random.randint(x,y) 

print("El valor generado fue ",numero)

# parametros actuales los que se colocan en el llamado del modulo
# error porque no coincide la cantidad de parametros actuales con los formales
numero = random.randint() 
numero = random.randint(1) 

# error porque no coinciden los tipos
numero = random.randint("1",5) 

# error porque no coincide el orden
numero = random.randint(5,1) 

