# los parametros formales son: nombre y sueldo
def mostrar_salidas(nombre,sueldo):
    print("El cliente ",nombre," obtendra un sueldo de ",sueldo,"$")
# fin del modulo

def leer_entradas(nombre,sueldo):
    nombre = input("Introduzca el nombre:")
    sueldo = input("Introduzca el sueldo:")
    
# cuerpo principal
n = "isaac"
s = 200

for i in range(1,30):
    mostrar_salidas(n,s)
    leer_entradas(n,s)
    mostrar_salidas(n,s)

# reusabilidad: llamar el mismo modulo varias veces con valores diferentes
#mostrar_salidas(250,"omar") # error porque no se esta respetando el orden
#mostrar_salidas("jose luis",500)
#mostrar_salidas("norka",550)
#mostrar_salidas(n,s)
#mostrar_salidas(nombre, sueldo)