# los parametros formales son: nombre y sueldo
def mostrar_salidas(nombre,sueldo):
    print("El cliente ",nombre," obtendra un sueldo de ",sueldo,"$")
# fin del modulo

def leer_entradas():
    # nombre y sueldo son variables locales
    nombre = input("Introduzca el nombre:")
    sueldo = input("Introduzca el sueldo:")
    # funcion que retorna 2 valores
    return nombre,sueldo

# cuerpo principal
n = "isaac"
s = 200

mostrar_salidas(n,s)
for i in range(1,3):
    # variables que reciben los valores que retorna 
    n,s = leer_entradas()
    mostrar_salidas(n,s)
