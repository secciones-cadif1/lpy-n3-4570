# Una agencia de carros paga a su personal de ventas 
# un salario base de 500.000 Bs. m�s una comisi�n de 200.000 Bs. 
# por cada autom�vil vendido, m�s el 10% del valor total de 
# las ventas. Si se tiene como entrada el nombre del vendedor, 
# el n�mero de autos vendidos y el valor total de sus ventas. 
# Se pide calcular e imprimir el sueldo que debe percibir 
# el vendedor.


def mostrar_salidas(nombre_vendedor,sueldo_final):
    print("")
    print("##############################################")
    print("El sueldo final del vendedor ",nombre_vendedor.upper(), end="")
    print(" es ",sueldo_final,"$")
    print("##############################################")
    input()

def calcular_salario_vendedor(valor_ventas,nro_autos_vendidos):
    comision_x_autos_vend = 200*nro_autos_vendidos
    comision_x_ventas = valor_ventas*0.10
    return 500+comision_x_autos_vend+comision_x_ventas

def mostrar_encabezado():
	print("############################################")
	print("###### Sistema de control de salarios ######")
	print("############################################")
	print("")
     
def leer_entradas():  
    mostrar_encabezado()

    print("Introduzca el nombre del vendedor:", end="")
    nombre = input()
    print("introduzca el número de autos vendidos:", end="")
    nro_autos = int(input())
    print("Introduzca el valor de las ventas en $:", end="")
    ventas = float(input())
    
    return nombre,nro_autos,ventas


#cuerpo principal del programa
nombre_vendedor,nro_autos_vendidos,valor_ventas = leer_entradas()
sueldo_final = calcular_salario_vendedor(valor_ventas,nro_autos_vendidos)
mostrar_salidas(nombre_vendedor,sueldo_final)

