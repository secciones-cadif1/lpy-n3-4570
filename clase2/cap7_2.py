
def leer_entradas():
    # n,a y v son 3 variables locales
    n = input("Introduzca el nombre del vendedor:")
    a = int(input("Introduzca los años de servicio:"))
    v = float(input("Introduzca el monto de sus ventas:"))
    
    # se retornan los 3 valores
    return n,a,v

def calcular_comision_basica(monto_ventas):
    if monto_ventas < 50000:
        return monto_ventas*0.05
    else:
        if monto_ventas < 100000:
            return monto_ventas*0.07
        else:
            if monto_ventas < 1000000:
                return monto_ventas*0.08
            else:
                return monto_ventas*0.1

def calcular_comision_antiguedad(años):
    if años > 15:
        return 5*(años-15)
    else:
        return 0

def mostrar_salidas(nom,com):
    print("El empleado ",nom.upper()," recibira una comision de ",com,"$")

# cuerpo principal
while True:
    # entradas
    nombre, años, ventas = leer_entradas()
    # el proceso
    comision_basica = round(calcular_comision_basica(ventas),1)
    comision_antiguedad = calcular_comision_antiguedad(años)
    comision_total = comision_basica + comision_antiguedad
    # salidas
    mostrar_salidas(nombre,comision_total)

    continuar = input("Desea continuar? (S/N)")
    if continuar.upper() == "N":
        break

def calcular_comision_basica_ver2(monto_ventas):
    if monto_ventas < 50000:
        monto = monto_ventas*0.05
    else:
        if monto_ventas < 100000:
            monto = monto_ventas*0.07
        else:
            if monto_ventas < 1000000:
                monto = monto_ventas*0.08
            else:
                monto = monto_ventas*0.1
    return round(monto,1)