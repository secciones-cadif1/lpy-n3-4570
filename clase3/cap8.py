
def calcular_area_triangulo():
    print("Seleccionó la opción 1")
    base = int(input("Introduzca la base del triangulo:"))
    altura = int(input("Introduzca la altura del triangulo:"))
    area = altura*base/2
    print("El area del triangulo es ",area)
    
def mostrar_menu():
    print("Menu de opciones:")
    print("1. Calcular área de un triangulo")
    print("2. Calcular área de un rectángulo")
    print("3. Calcular área de un círculo")
    print("4. Salir")
    print("Introduzca la opción que desea ejecutar:")
    return int(input()) # retorna el valor leido

# cuerpo principal
while True:
    opcion = mostrar_menu() 
    match opcion:
        case 1: 
            calcular_area_triangulo()
        case 2:
            print("selecciono la opcion 2")
        case 3:
            print("selecciono la opcion 3")
        case 4:
            print("selecciono la opcion salir")
            break
        case _:
            print("Opcion errada. Intente de nuevo")