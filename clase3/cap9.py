# funcion para leer datos de entrada
def leer_datos(i):
    print("Introduzca el nombre del jugador ",i,":")
    n = input()
    print("Introduzca la estatura del jugador ",i,"(en cm):")
    e = float(input())
    return n,e

# cuerpo principal. IMPORTANTE: Este algoritmo no resolvera apropiadamente
# el caso en el que hayan valores mayores iguales, es decir, cuando haya empates
# para ese caso, necesitamos arreglos
auxiliar = 0
# se incializan los contadores antes del ciclo
cant_may_2 = 0
cant_men_150 = 0
for i in range(1,5):
    nombre, estatura = leer_datos(i)
    # se actualizan los contadores adentro del ciclo
    if estatura > 200:
        cant_may_2 = cant_may_2 + 1
    else:
        if estatura < 150:
            cont_men_150 = cont_men_150 + 1

    if estatura > auxiliar:
        auxiliar = estatura
        nombre_mayor = nombre
# fin del ciclo
print("El numero de atletas con mas de 2 mts es ",cant_may_2)
print("el numero de atletas con menos de 1.5mts es ",cont_men_150)
# al terminar el ciclo, mostramos el mayor valor y quien lo tien
print("La mayor estatura la tiene ",nombre_mayor," y es ",auxiliar,"cm")
input()